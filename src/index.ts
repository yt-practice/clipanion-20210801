import { Builtins, Cli, Command, Option } from 'clipanion'
import * as t from 'typanion'

class HelloCommand extends Command {
	name = Option.String({ required: true })
	static paths = [['hello']]
	static usage = { description: '挨拶します' }
	async execute() {
		this.context.stdout.write(`Hello ${this.name}!\n`)
	}
}

class PowerCommand extends Command {
	a = Option.String({ validator: t.isNumber() })
	b = Option.String({ validator: t.isNumber() })
	static paths = [['power']]
	static usage = { description: '冪乗の計算' }
	async execute() {
		this.context.stdout.write(`${this.a ** this.b}\n`)
	}
}

const [node, app, ...args] = process.argv

const cli = new Cli({
	binaryLabel: `My Application`,
	binaryName: `${node} ${app}`,
	binaryVersion: `1.0.0`,
})

cli.register(Builtins.HelpCommand)
cli.register(HelloCommand)
cli.register(PowerCommand)
cli.runExit(args, Cli.defaultContext)
